package com.nutryphore.Entidades;

import android.widget.ImageView;

public class Conceptos {
    private String nombreConcepto;
    private String definicionConcepto;
    private int imagenConcepto;

    public Conceptos(String nombreConcepto, String definicionConcepto, int imagenConcepto) {
        this.nombreConcepto = nombreConcepto;
        this.definicionConcepto = definicionConcepto;
        this.imagenConcepto = imagenConcepto;
    }

    public String getNombreConcepto() {
        return nombreConcepto;
    }

    public void setNombreConcepto(String nombreConcepto) {
        this.nombreConcepto = nombreConcepto;
    }

    public String getDefinicionConcepto() {
        return definicionConcepto;
    }

    public void setDefinicionConcepto(String definicionConcepto) {
        this.definicionConcepto = definicionConcepto;
    }

    public int getImagenConcepto() {
        return imagenConcepto;
    }

    public void setImagenConcepto(int imagenConcepto) {
        this.imagenConcepto = imagenConcepto;
    }
}
