package com.nutryphore.Entidades;

public class TipoEtiquetado {
    private int idRecursoImagen;
    private String tipoEtiquetado;
    private String descripcionEtiquetado;

    public TipoEtiquetado(int idRecursoImagen, String tipoEtiquetado, String descripcionEtiquetado) {
        this.idRecursoImagen = idRecursoImagen;
        this.tipoEtiquetado = tipoEtiquetado;
        this.descripcionEtiquetado = descripcionEtiquetado;
    }

    public int getIdRecursoImagen() {
        return idRecursoImagen;
    }

    public void setIdRecursoImagen(int idRecursoImagen) {
        this.idRecursoImagen = idRecursoImagen;
    }

    public String getTipoEtiquetado() {
        return tipoEtiquetado;
    }

    public void setTipoEtiquetado(String tipoEtiquetado) {
        this.tipoEtiquetado = tipoEtiquetado;
    }

    public String getDescripcionEtiquetado() {
        return descripcionEtiquetado;
    }

    public void setDescripcionEtiquetado(String descripcionEtiquetado) {
        this.descripcionEtiquetado = descripcionEtiquetado;
    }
}
