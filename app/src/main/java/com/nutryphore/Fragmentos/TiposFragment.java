package com.nutryphore.Fragmentos;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nutryphore.Adaptadores.AdaptadorLocalizadorTipos;
import com.nutryphore.Entidades.TipoEtiquetado;
import com.nutryphore.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TiposFragment extends android.support.v4.app.Fragment {


    public TiposFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View mainActivity = inflater.inflate(R.layout.fragment_tipos, container, false);
        ViewPager localizadorTipos = (ViewPager) mainActivity.findViewById(R.id.vp_tipos);
        AdaptadorLocalizadorTipos adaptadorLocalizadorTipos = new AdaptadorLocalizadorTipos(mainActivity.getContext(), CargarListadoTiposEtiquetado());
        localizadorTipos.setAdapter(adaptadorLocalizadorTipos);
        return mainActivity;
    }

    private ArrayList<TipoEtiquetado> CargarListadoTiposEtiquetado() {
        ArrayList<TipoEtiquetado> listadoTiposEtiquetado = new ArrayList<TipoEtiquetado>();
        listadoTiposEtiquetado.add(new TipoEtiquetado(R.drawable.tipos_1, "1. Formato vertical estándar: ", "El formato vertical estándar es el estilo más utilizado se caracteriza por tener cinco segmentos y presentar la información nutricional de manera vertical."));
        listadoTiposEtiquetado.add(new TipoEtiquetado(R.drawable.tipos_2, "2. Formato con declaración lateral: ", "El formato con declaración lateral se puede utilizar cuando el espacio debajo de la información de vitaminas y minerales no es suficiente para incluir el valor diario y la información sobre conversiones calóricas"));
        listadoTiposEtiquetado.add(new TipoEtiquetado(R.drawable.tipos_3, "3. Formato con declaración dual: ", "El formato con declaración dual puede ser usado para dos o más formas del mismo alimento, por ejemplo, “tal como se compra“ o “preparado”, para una combinación común de alimentos (adición de otro ingrediente), para diferentes unidades (una rebanada de pan o por 100 gramos)."));
        listadoTiposEtiquetado.add(new TipoEtiquetado(R.drawable.tipos_4, "4. Formato simplificado: ", "El formato simplificado, puede ser usado cuando un alimento contiene cantidades no significativas o no es fuente significativa de ocho (8) o más de los siguientes datos nutricionales: calorías/kilocalorías, grasa total, grasa saturada, grasa trans, colesterol, sodio, carbohidratos totales, fibra dietaria, azúcares, proteína, vitamina A, vitamina C, calcio y hierro. Para este efecto “cantidad no significativa” o “no es fuente significativa”."));
        listadoTiposEtiquetado.add(new TipoEtiquetado(R.drawable.tipos_5, "5. Formato tabular: ", "El Formato Tabular debe presentar la información nutricional en forma horizontal dispuesta en tres secciones."));
        listadoTiposEtiquetado.add(new TipoEtiquetado(R.drawable.tipos_6, "6. Formato lineal: ", "El formato lineal se puede utilizar solamente cuando no es posible incluir en la etiqueta el formato tabular. Los ingredientes deben estar seguidos, separados por comas. En este formato, el porcentaje de Valor Diario (% VD) debe declararse entre paréntesis, después del nutriente respectivo utilizando las abreviaturas permitidas"));
        return listadoTiposEtiquetado;
    }

}
