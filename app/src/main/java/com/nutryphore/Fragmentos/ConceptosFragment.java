package com.nutryphore.Fragmentos;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.nutryphore.Adaptadores.AdapterConceptos;
import com.nutryphore.Entidades.Conceptos;
import com.nutryphore.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConceptosFragment extends android.support.v4.app.Fragment {
    private ListView lvConceptos;
    private AdapterConceptos adaptadorConceptos;

    public ConceptosFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_conceptos, container, false);
        lvConceptos = (ListView) vista.findViewById(R.id.lv_lista_conceptos);
        adaptadorConceptos = new AdapterConceptos(vista.getContext(), CargarListaConceptos());
        lvConceptos.setAdapter(adaptadorConceptos);
        return vista;
    }

    private ArrayList<Conceptos> CargarListaConceptos(){
        ArrayList<Conceptos> listaConceptos = new ArrayList<Conceptos>();
        listaConceptos.add(new Conceptos("Dieta", "Alimentación habitual de una persona o población.", R.drawable.conceptos_dieta));
        listaConceptos.add(new Conceptos("Kilocalorias", "Son la unidad de medida de cuanta energía aporta un alimento o un producto.", R.drawable.conceptos_kilocalorias));
        listaConceptos.add(new Conceptos("Carbohidratos", "Todos los mono, di, oligo y polisacáridos, incluidos los polialcoholes presentes en el alimento.", R.drawable.conceptos_carbohidratos));
        listaConceptos.add(new Conceptos("Grasa Total", "Sumatoria de grasa saturada, grasa monoinsaturada, grasa poliinsaturada(incluidas las grasas trans).", R.drawable.conceptos_grasa_total));
        listaConceptos.add(new Conceptos("Proteína", "Son polímeros de l-α aminoácidos unidos por enlaces peptídicos.", R.drawable.conceptos_proteinas));
        listaConceptos.add(new Conceptos("Colesterol", "Sustancia tipo esterol presente en las grasas de origen animal.", R.drawable.conceptos_colesterol));
        listaConceptos.add(new Conceptos("Vitaminas", "Sustancias orgánicas esenciales para el mantenimiento de la salud, crecimiento y funcionamiento corporal normal.", R.drawable.conceptos_vitaminas));
        listaConceptos.add(new Conceptos("Minerales", "Sustancias inorgánicas necesarias para los procesos fisiológicos y que no son fuente de energía.", R.drawable.conceptos_minerales));
        listaConceptos.add(new Conceptos("Rotulado", "Toda descripción contenida en el rótulo o etiqueta de un alimento destinada a informar al consumidor sobre el contenido de nutrientes, propiedades nutricionales y propiedades de salud de un alimento.", R.drawable.conceptos_rotulado));
        listaConceptos.add(new Conceptos("Porcentaje De Valor Diario (%Vd)", "El aporte que hace al valor de referencia un determinado nutriente presente en un alimento, expresado en porcentaje.", R.drawable.conceptos_porcentaje_valor_diario));
        return listaConceptos;
    }

}
