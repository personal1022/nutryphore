package com.nutryphore.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nutryphore.Entidades.TipoEtiquetado;
import com.nutryphore.R;

import java.util.ArrayList;

public class AdaptadorLocalizadorTipos extends PagerAdapter {
    private Context contexto;
    private LayoutInflater layoutInflater;
    private ArrayList<TipoEtiquetado> listaEtiquetado;

    public AdaptadorLocalizadorTipos(Context contexto, ArrayList<TipoEtiquetado> listaEtiquetado) {
        this.contexto = contexto;
        this.listaEtiquetado = listaEtiquetado;
    }

    @Override
    public int getCount() {
        return listaEtiquetado.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) contexto.getSystemService(contexto.LAYOUT_INFLATER_SERVICE);
        View vista = layoutInflater.inflate(R.layout.item_tipos_etiqueta, null);
        ImageView imgVistaTiposEtiqueta = (ImageView) vista.findViewById(R.id.img_tipos_vista_defecto);
        TextView txtTitulo = (TextView) vista.findViewById(R.id.txt_tipo_titulo);
        TextView txtDescripcion = (TextView) vista.findViewById(R.id.txt_tipo_descripcion);
        imgVistaTiposEtiqueta.setImageResource(listaEtiquetado.get(position).getIdRecursoImagen());
        txtTitulo.setText(listaEtiquetado.get(position).getTipoEtiquetado());
        txtDescripcion.setText(listaEtiquetado.get(position).getDescripcionEtiquetado());

        ViewPager localizadorTipos = (ViewPager) container;
        localizadorTipos.addView(vista, 0);
        return vista;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager localizadorTipos = (ViewPager) container;
        View vista = (View) object;
        localizadorTipos.removeView(vista);
    }
}


