package com.nutryphore.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nutryphore.Entidades.Conceptos;
import com.nutryphore.R;

import java.util.ArrayList;

public class AdapterConceptos extends BaseAdapter {
    private Context contexto;
    private ArrayList<Conceptos> listaConceptos;

    public AdapterConceptos(Context contexto, ArrayList<Conceptos> listaConceptos) {
        this.contexto = contexto;
        this.listaConceptos = listaConceptos;
    }

    @Override
    public int getCount() {
        return listaConceptos.size();
    }

    @Override
    public Object getItem(int position) {
        return listaConceptos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Conceptos conceptos = (Conceptos) getItem(position);
        convertView = LayoutInflater.from(contexto).inflate(R.layout.item_lista_conceptos, null);
        ImageView imgConcepto = (ImageView) convertView.findViewById(R.id.img_concepto);
        TextView txtConceptoTitulo = (TextView) convertView.findViewById(R.id.txt_concepto_titulo);
        TextView txtConceptoDescripcion = (TextView) convertView.findViewById(R.id.txt_concepto_descripcion);

        imgConcepto.setImageResource(conceptos.getImagenConcepto());
        txtConceptoTitulo.setText(conceptos.getNombreConcepto());
        txtConceptoDescripcion.setText(conceptos.getDefinicionConcepto());
        return convertView;
    }
}
